document.addEventListener("DOMContentLoaded", function(event) {		  	
	showRequests();
    document.getElementById('search_button').onclick=function(){
		sendParse(document.getElementById('search').value);
	};
});

function sendParse(search_value){
	if (search_value) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            	showRequests();
            }
        };
        xmlhttp.open("GET","parse.php?search_value="+search_value,true);
        xmlhttp.send();
    }
}

function showRequests(){
	if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        	var requests = xmlhttp.response;
        	var out = "";
        	for(i = 0; i < requests.length; i++) {
        		out += '<span class="request" id="' + requests[i].id + '">' + requests[i].request + '</span><br>';
        	}
        	document.getElementById("container_one").innerHTML = out;
        }
        var el = document.getElementsByClassName('request');
		for (var i=0;i<el.length; i++) {
		    el[i].onclick=function(){showResponses(this.id);};
		}
    };
    xmlhttp.open("GET","ajax.php?type=getrequests",true);
    xmlhttp.responseType = 'json';
    xmlhttp.send();
}

function showResponses(str) {
    if (str == "") {
        document.getElementById("results_table").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            	var obj = xmlhttp.response;
            	createTable(xmlhttp.response);
            }
        };
        xmlhttp.open("GET","ajax.php?type=getresponses&q="+str,true);
        xmlhttp.responseType = 'json';
        xmlhttp.send();
    }
}

function createTable(response) {
    var i;
    var out = '<tr><th>ID</th><th>Title</th><th>Description</th><th>Rating</th><th>Request</th></tr>';

    for(i = 0; i < response['third'].length; i++) {
        out += "<tr><td>" +
        response['third'][i].id +
        "</td><td>" +
        response['third'][i].title +
        "</td><td>" +
        response['third'][i].description +
        "</td><td>" +
        response['third'][i].rating +
		"</td><td>" +
        response['third'][i].request_id +
        "</td></tr>";
    }
    out += "</table>";
    document.getElementById("results_table").innerHTML = out;

    var infoBlock = 'Count response : '+response['third'].length+
    	'<br>Request : '+ response['section_second']['request'][0][0]+
    	'<br> AVG rating : '+response['section_second']['avg'][0][0];
    document.getElementById("container_second").innerHTML = infoBlock;

}