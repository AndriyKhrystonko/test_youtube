<?php
header("Content-Type: application/json; charset=UTF-8");
include('db_config.php');
$type = isset($_GET['type']) ? $_GET['type'] : '';

switch ($type) {
    case 'getresponses':
        $result = getResponses($conn);
        break;
    case 'getrequests':
        $result = getRequests($conn);
        break;
}

echo json_encode($result);

function getRequests($conn){
    $sth = $conn->prepare('SELECT * FROM requests ORDER BY id');
    $sth->execute();
    return $sth->fetchAll();
}

function getResponses($conn){
    $request_id = isset($_GET['q']) ? intval($_GET['q']) : 0;

    $sth = $conn->prepare('SELECT `id`,`title`,`description`,`rating`,`request_id` FROM `results` WHERE `request_id` = '.$request_id.' ORDER BY `id`');
    $sth->execute();
    $result['third'] = $sth->fetchAll();

    $sth = $conn->prepare('SELECT `request` FROM `requests` WHERE `id` = '.$request_id);
    $sth->execute();
    $result['section_second']['request'] = $sth->fetchAll();
    $sth = $conn->prepare('SELECT avg(rating) FROM `results` WHERE `request_id` = '.$request_id);
    $sth->execute();
    $result['section_second']['avg'] = $sth->fetchAll();
    return $result;
}



?>