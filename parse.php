<?php
	include('db_config.php');
	header('Content-Type: text/html; charset=utf-8');
	$requests = isset($_GET['search_value']) ? $_GET['search_value'] : exit();
	//$requests = 'adriano celentano, sting, pavarotti caruso, celine dion, michael jackson';
	$requests = explode(",",$requests);

	foreach ($requests as $key => $request) {
		$request = trim($request);
		
		$search_request = str_replace(" ", "+", $request);
		$ch = curl_init();

	    curl_setopt($ch, CURLOPT_URL, 'https://www.youtube.com/results?search_query='.$search_request.'&sp=EgIQAQ%253D%253D' );
		
		curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,10);
	    curl_setopt($ch, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
	    
	    curl_setopt($ch, CURLOPT_POST, 0);
	    curl_setopt($ch, CURLOPT_HTTPGET, 1);

	    $return = curl_exec ($ch);
		curl_close ($ch);

		$stmt = $conn->prepare("INSERT INTO `requests` (`request`) VALUES (:request)");
		$stmt->bindParam(':request', $request);
		try {
			$stmt->execute();
		}  catch (Exception $e){
			continue;
		}
		$requestId = (int) $conn->lastInsertId();
		if (preg_match_all("!yt-lockup-content(.*?)yt-lockup yt-lockup-tile!si", $return, $items)) {
			$firstTenItems = array_slice($items[1], 0, 10);
			foreach ($firstTenItems as $key => $item) {
				preg_match("!dir=\"ltr\">(.*?)</a>!si", $item, $title);
				preg_match("!ellipsis-2\" dir=\"ltr\">(.*?)</div>!si", $item, $description);
				preg_match("!</li><li>(.*?)</li>!si", $item, $rating);
				$title = $title[1];
				$description = isset($description[1]) ? $description[1] : '-';
				$rating = preg_replace("/[^0-9,.]/", "", $rating[1]);

				$stmt = $conn->prepare("INSERT INTO `results` (`title`,`description`,`rating`,`request_id`) VALUES (:title,:description,:rating,:request_id)");
				$stmt->bindParam(':title', $title);
				$stmt->bindParam(':description', $description);
				$stmt->bindParam(':rating', $rating);
				$stmt->bindParam(':request_id', $requestId,  PDO::PARAM_INT);
				
				$stmt->execute();
			}
		}
	}
?>